# OpenML dataset: PhosphoproteinChallenge_DREAM3

https://www.openml.org/d/43284

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

//Add the description.md of the data file PhosphoproteinChallenge_DREAM3

Human normal and cancer hepatocytes (cell line HepG2s) were treated with 7 stimuli (Table 1a) that are relevant to hepatocyte physiology. For each applied stimulus, 7 selective inhibitors (Table 1b) that block the activity of specific molecules have been applied independently (i.e., only one inhibitor at a time). For each combination of stimulus-inhibitor, the concentration of 17 intracellular phospho-protein molecules (Table 1c) were measured at three time points (0, 30min, 3hours) after stimulation. Also for each combination of stimulus-inhibitor the extra-cellular concentration of 20 cytokines (Table 1d) released by the cells were measured at 3 time points (0, 3hrs, 24hrs) after stimulation. The experimental design is shown schematically in Figure 1, where the data for either a phospho-protein or a cytokine data is exemplified.


The data is contained in two spreadsheets, one for the phosphorylation data (PhosphoproteinChallengeDREAM3.csv) and one for the cytokine release data (CytokineChallengeDREAM3.csv). The data is structured according to the following format: in both files the first column contains the cell type (Normal or Cancer), the second column specifies the stimulus, the third column lists the inhibitor, and the fourth column contains the time of data acquisition in minutes. From column 5 to 21, the file PhosphoproteinChallengeDREAM3.csv contains the abundance of the 17 phospho-proteins in arbitrary fluorescence units and in the order given in Table 1c. From column 5 to 24, the file PhosphoproteinChallengeDREAM3.csv contains the abundance of the 20 measured extracellular cytokines in arbitrary fluorescence units and in the order given in Table 1d. The values that have to be predicted have been replaced in the data files by the text: "PREDICT".

Useful Information regarding measurements
(a) Data integrity / linearity. Significant effort was dedicated to data integrity. The data are reported as arbitrary (fluorescence) units in the range between 0 and 29000. The upper limit (29000) corresponds to the saturation limit of the detector. Experiments were performed in such a way that measurements are as much as possible within the linear range of the detector. In general, data can be considered linear but there are a few cases that measurements are closer to the upper detection limit of ~29000 (e.g. some cJUN and IL8 measurements) where linearity might have been lost.
(b) Detection limits/Repeatability. The coefficient of variation for repeated measurements was found to be approx. 8 percent (mostly due to biological error). With our current experimental design the instrument detector can report data with accuracy as low as ~300. For example, changes from 55 fluorescence units (FU) to 110 FU cannot be considered "2 fold increase" because values lie within the noise error of the detector. On the contrary, data from 1000 to 2000 are significant.
(c) Inhibitor effects. There are cases in which our inhibitors (i.e. MEKi, p38i, and JNKi) target molecules whose phosphorylation we measure (i.e. MEK12, p38, and JNK). In the case where the inhibitor is present, the phosphorylation state of the corresponding molecule (i.e. phospho-MEK, phospho-p38, and phospho-JNK) should be assumed "absent" and the phosphorylation value should not be used. This known inhibitor effect is more pronounced on the allosteric inhibitors (i.e. the effect of MEK inhibitor on the MEK phosphorylation). The effects of the inhibitors are indirectly corroborated from the phosphorylation state of their downstream targets (i.e. MEK -> ERK, p38 -> HSP27, JNK -> cJUN).
Additional data
Any additional prior data already present in the literature can be used. This could be especially useful if a model of the network is needed as part of a method to predict the excluded data.
About the Data
Data generously provided by Peter Sorger, Harvard Medical School / MIT
See

[Link](http://wiki.c2b2.columbia.edu/dream/data/scripts/DREAM3/)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43284) of an [OpenML dataset](https://www.openml.org/d/43284). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43284/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43284/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43284/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

